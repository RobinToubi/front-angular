import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { CharacterComponent } from './character/character.component';
import { FormCharacterComponent } from './components/FormCharacter/FormCharacter.component';
import { MovieListComponent } from './components/movie-list/movie-list.component';

const routes: Routes = [
  {
    path: 'characters',
    component: CharacterComponent,
  },
  {
    path: 'characters/create',
    component: FormCharacterComponent,
  },
  {
    path: 'movies',
    component: MovieListComponent,
  },
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule],
})
export class AppRoutingModule {}
