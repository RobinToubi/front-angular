import { Component } from '@angular/core';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss'],
})
export class AppComponent {
  title = 'front';

  characters: Character[] = [
    {
      nom: 'Harry',
      prenom: 'Potter',
    },
    {
      nom: 'Albus',
      prenom: 'Dumbledore',
    },
  ];

  displayedColumns: string[] = ['nom', 'prenom'];
  dataSource = this.characters;
}

export interface Character {
  nom: string;
  prenom: string;
}
