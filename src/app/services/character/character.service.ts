import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';
import { map } from 'rxjs/operators';
import { ICharacter } from 'src/app/character/icharacter';

@Injectable({
  providedIn: 'root',
})
export class CharacterService {
  constructor(private http: HttpClient) {}

  findAll(): Observable<ICharacter[]> {
    return this.http.get<ICharacter[]>('http://localhost:3000/characters');
  }

  create(character: ICharacter): Observable<ICharacter> {
    return this.http.post<ICharacter>(
      'http://localhost:3000/characters',
      character
    );
  }

  delete(id: number): Observable<any> {
    return this.http.delete('http://localhost:3000/characters/' + id);
  }
}
