import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';
import { IMovie } from 'src/app/components/movie/IMovie';

@Injectable({
  providedIn: 'root',
})
export class MovieService {
  constructor(private http: HttpClient) {}

  findAll(): Observable<IMovie[]> {
    return this.http.get<IMovie[]>('http://localhost:3000/movies');
  }

  delete(id: number): Observable<any> {
    return this.http.delete('http://localhost:3000/movies/' + id);
  }
}
