import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { CharacterComponent } from './character.component';
import { MatTableModule } from '@angular/material/table';
import { FormCharacterComponent } from '../components/FormCharacter/FormCharacter.component';
import { ReactiveFormsModule } from '@angular/forms';
import { HttpClientModule } from '@angular/common/http';
import { AppRoutingModule } from '../app-routing.module';
import { MatButtonModule } from '@angular/material/button';
import { MatFormFieldModule } from '@angular/material/form-field';
import { MatInputModule } from '@angular/material/input';
import { MatIconModule } from '@angular/material/icon';

@NgModule({
  declarations: [CharacterComponent, FormCharacterComponent],
  imports: [
    AppRoutingModule,
    CommonModule,
    MatTableModule,
    ReactiveFormsModule,
    MatButtonModule,
    HttpClientModule,
    MatFormFieldModule,
    MatInputModule,
    MatIconModule,
  ],
  exports: [CharacterComponent],
})
export class CharacterModule {}
