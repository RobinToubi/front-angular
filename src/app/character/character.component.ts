import { Component, OnInit } from '@angular/core';
import { CharacterService } from '../services/character/character.service';
import { ICharacter } from './icharacter';

@Component({
  selector: 'app-character',
  templateUrl: './character.component.html',
  styleUrls: ['./character.component.scss'],
  providers: [CharacterService],
})
export class CharacterComponent implements OnInit {
  characters: ICharacter[];
  dataSource: any;
  displayedColumns: string[] = ['nom', 'prenom', 'age', 'deleteRow'];

  constructor(private service: CharacterService) {}

  ngOnInit() {
    this.service.findAll().subscribe((characters) => {
      this.dataSource = characters;
    });
  }

  delete(id: number) {
    this.service.delete(id).subscribe(
      (response) => {
        this.ngOnInit();
      },
      (error) => {
        console.log(error);
      }
    );
  }
}
