import { Routes, RouterModule } from '@angular/router';
import { FormCharacterComponent } from '../components/FormCharacter/FormCharacter.component';

const routes: Routes = [
  {
    path: 'create',
    component: FormCharacterComponent,
  },
];

export const CharactersRoutes = routes;
