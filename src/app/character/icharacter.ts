export interface ICharacter {
  id?: number;
  firstName: string;
  lastName: string;
  birthYear?: number;
  age: number;
}
