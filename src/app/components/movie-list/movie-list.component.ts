import { Component, OnInit } from '@angular/core';
import { IMovie } from '../movie/IMovie';
import { MovieService } from '../../services/movie/movie.service';

@Component({
  selector: 'app-movie-list',
  templateUrl: './movie-list.component.html',
  styleUrls: ['./movie-list.component.scss'],
  providers: [MovieService],
})
export class MovieListComponent implements OnInit {
  movies: IMovie[];
  constructor(private service: MovieService) {}

  ngOnInit() {
    this.service.findAll().subscribe(
      (response) => {
        console.log(response);
        this.movies = response;
      },
      (error) => {
        console.log(error);
      }
    );
  }

  delete(id: number) {
    this.service.delete(id).subscribe(
      (response) => {
        this.service.findAll().subscribe(
          (response) => {
            this.movies = response;
          },
          (error) => {
            console.log(error);
          }
        );
      },
      (error) => {
        console.log(error);
      }
    );
  }
}
