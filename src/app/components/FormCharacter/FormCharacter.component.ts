import { Component, OnInit } from '@angular/core';
import { FormControl, FormGroup } from '@angular/forms';
import { Router } from '@angular/router';
import { ICharacter } from '../../character/icharacter';
import { CharacterService } from '../../services/character/character.service';

@Component({
  selector: 'app-FormCharacter',
  templateUrl: './FormCharacter.component.html',
  styleUrls: ['./FormCharacter.component.scss'],
})
export class FormCharacterComponent implements OnInit {
  form: FormGroup;

  constructor(
    private characterService: CharacterService,
    private router: Router
  ) {
    this.form = new FormGroup({
      firstName: new FormControl(),
      lastName: new FormControl(),
      age: new FormControl(),
    });
  }

  ngOnInit() {}

  create() {
    const character: ICharacter = {
      firstName: this.form.value.firstName,
      lastName: this.form.value.lastName,
      age: this.form.value.age,
    };
    this.characterService
      .create(character)
      .subscribe((createdCharacter) => this.router.navigate(['/characters']));
  }

  save(): void {
    this.form.patchValue({
      firstName: 'Harry',
    });
  }

  reset(): void {
    this.form.reset();
  }
}
