import { IMovie } from './IMovie';
import { Component, EventEmitter, Input, Output } from '@angular/core';
import { MovieService } from '../../services/movie/movie.service';

@Component({
  selector: 'app-movie',
  templateUrl: './movie.component.html',
  styleUrls: ['./movie.component.scss'],
  providers: [MovieService],
})
export class MovieComponent {
  @Input() movie: IMovie;
  @Output() deleteMovie: EventEmitter<number> = new EventEmitter();
  errorMessage: string;
  status: string;

  constructor(private service: MovieService) {}

  delete(id: number) {
    this.deleteMovie.emit(id);
  }
}
