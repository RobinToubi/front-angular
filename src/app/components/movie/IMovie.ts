export interface IMovie {
  id: number;
  name: string;
  date: Date;
  affiche: string;
}
