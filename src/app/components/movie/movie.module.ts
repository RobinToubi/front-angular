import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { MatCardModule } from '@angular/material/card';
import { MovieComponent } from './movie.component';
import { MovieListComponent } from '../movie-list/movie-list.component';
import { MatIconModule } from '@angular/material/icon';
import { MatButtonModule } from '@angular/material/button';

@NgModule({
  declarations: [MovieComponent, MovieListComponent],
  imports: [MatCardModule, CommonModule, MatIconModule, MatButtonModule],
  exports: [MatIconModule],
})
export class MovieModule {}
